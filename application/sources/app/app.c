/*
 * FreeModbus Libary: BARE Demo Application
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */


#include "app.h"
#include "platform.h"

#include "stm32f0xx.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "port.h"

#include "io_cfg.h"
#include "xprintf.h"

#include "sht3x.h"
#include "sht3x_system.h"

/* ----------------------- Defines ------------------------------------------*/
#define REG_INPUT_START 1000
#define REG_INPUT_NREGS 4

/* ----------------------- Static variables ---------------------------------*/
static USHORT   usRegInputStart = REG_INPUT_START;
static USHORT   usRegInputBuf[REG_INPUT_NREGS];

/* ----------------------- Start implementation -----------------------------*/

void
app_dbg_fatal( const int8_t* s, uint8_t c )
{
	int delayVal = 100;
	(void)s;
	(void)c;
	while (1)
	{
		GPIO_SetBits(GPIOA, GPIO_Pin_4);
		vMBPortTimersDelay(delayVal);
		GPIO_ResetBits(GPIOA, GPIO_Pin_4);
		vMBPortTimersDelay(delayVal);
	}
}

#if 1
void
main_app( void )
{
	eMBErrorCode    eStatus;
	(void)eStatus;

	/* ----------------------- Test Porting -------------------------------------*/
	GPIO_InitTypeDef        GPIO_InitStructure;

	/* GPIOC Periph clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_SetBits(GPIOA, GPIO_Pin_4);

	eStatus = eMBInit( MB_RTU, 0x01, 0, 115200, MB_PAR_NONE );

	/* Enable the Modbus Protocol Stack. */
	eStatus = eMBEnable(  );

	for( ;; )
	{
		( void )eMBPoll(  );

		/* Here we simply count the number of poll cycles. */
		usRegInputBuf[0]++;
		usRegInputBuf[1] = usRegInputBuf[1] + 2;
		usRegInputBuf[2] = usRegInputBuf[2] + 2;
		usRegInputBuf[2] = 123;
	}
}
#else
void
main_app() {
	etError   error;       // error code
	u32t      serialNumber;// serial number
	ft        temperature; // temperature [°C]
	ft        humidity;    // relative humidity [%RH]

	uart1_init(115200);
	xprintf_stream_io_out = uart1_putc;
	xprintf("SHT3x Unit Test :) !!!\n");

	SHT3X_Init(0x45); // Address: 0x44 = Sensor on EvalBoard connector
	//          0x45 = Sensor on EvalBoard

	// wait 50ms after power on
	DelayMicroSeconds(50000);

	error = SHT3x_ReadSerialNumber(&serialNumber);
	if (error != NO_ERROR) {
		xprintf("SHT3x_ReadSerialNumber NG\n");
	}
	else {
		xprintf("SHT3x_ReadSerialNumber: %d\n", serialNumber);
	}

	// demonstrate a single shot measurement with clock-stretching
	error = SHT3X_GetTempAndHumi(&temperature, &humidity, REPEATAB_HIGH, MODE_CLKSTRETCH, 50);
	if (error != NO_ERROR) {
		xprintf("SHT3X_GetTempAndHumi(MODE_CLKSTRETCH) NG\n");
	}
	else {
		xprintf("SHT3X_GetTempAndHumi(MODE_CLKSTRETCH), temperature: %d, humidity: %d\n", temperature, humidity);
	}

	// demonstrate a single shot measurement with polling and 50ms timeout
	error = SHT3X_GetTempAndHumi(&temperature, &humidity, REPEATAB_HIGH, MODE_POLLING, 50);
	if (error != NO_ERROR) {
		xprintf("SHT3X_GetTempAndHumi(MODE_POLLING) NG\n");
	}
	else {
		xprintf("SHT3X_GetTempAndHumi(MODE_POLLING), temperature: %d, humidity: %d\n", temperature, humidity);
	}

	while (1) {

	}
}
#endif
eMBErrorCode
eMBRegInputCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs )
{
	eMBErrorCode    eStatus = MB_ENOERR;
	int             iRegIndex;

	if( ( usAddress >= REG_INPUT_START )
			&& ( usAddress + usNRegs <= REG_INPUT_START + REG_INPUT_NREGS ) )
	{
		iRegIndex = ( int )( usAddress - usRegInputStart );
		while( usNRegs > 0 )
		{
			*pucRegBuffer++ =
					( unsigned char )( usRegInputBuf[iRegIndex] >> 8 );
			*pucRegBuffer++ =
					( unsigned char )( usRegInputBuf[iRegIndex] & 0xFF );
			iRegIndex++;
			usNRegs--;
		}
	}
	else
	{
		eStatus = MB_ENOREG;
	}

	return eStatus;
}

eMBErrorCode
eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs,
				 eMBRegisterMode eMode )
{
	(void)pucRegBuffer;
	(void)usAddress;
	(void)usNRegs;
	(void)eMode;
	return MB_ENOREG;
}


eMBErrorCode
eMBRegCoilsCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils,
			   eMBRegisterMode eMode )
{
	(void)pucRegBuffer;
	(void)usAddress;
	(void)usNCoils;
	(void)eMode;
	return MB_ENOREG;
}

eMBErrorCode
eMBRegDiscreteCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete )
{
	(void)pucRegBuffer;
	(void)usAddress;
	(void)usNDiscrete;
	return MB_ENOREG;
}
