/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   05/09/2016
 ******************************************************************************
**/
#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "app.h"
#include "platform.h"

#define FATAL(s, c) \
	do { \
		DISABLE_INTERRUPTS(); \
		(void)s; \
		(void)c; \
		app_dbg_fatal((const int8_t*)s, (uint8_t)c); \
} while (0)

#ifdef __cplusplus
}
#endif

#endif //__SYSTEM_H__
