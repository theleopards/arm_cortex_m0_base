CFLAGS += -I./sources/platform/mb_port
CFLAGS += -I./sources/libraries/freemodbus/modbus/
CFLAGS += -I./sources/libraries/freemodbus/modbus/rtu
CFLAGS += -I./sources/libraries/freemodbus/modbus/ascii
CFLAGS += -I./sources/libraries/freemodbus/modbus/include

VPATH += sources/platform/mb_port
VPATH += sources/libraries/freemodbus/modbus/
VPATH += sources/libraries/freemodbus/modbus/rtu
VPATH += sources/libraries/freemodbus/modbus/ascii
VPATH += sources/libraries/freemodbus/modbus/functions

SOURCES += sources/platform/mb_port/portserial.c
SOURCES += sources/platform/mb_port/portevent.c
SOURCES += sources/platform/mb_port/porttimer.c

SOURCES += sources/libraries/freemodbus/modbus/mb.c
SOURCES += sources/libraries/freemodbus/modbus/rtu/mbrtu.c
SOURCES += sources/libraries/freemodbus/modbus/rtu/mbcrc.c
SOURCES += sources/libraries/freemodbus/modbus/ascii/mbascii.c
SOURCES += sources/libraries/freemodbus/modbus/functions/mbfunccoils.c
SOURCES += sources/libraries/freemodbus/modbus/functions/mbfuncdiag.c
SOURCES += sources/libraries/freemodbus/modbus/functions/mbfuncholding.c
SOURCES += sources/libraries/freemodbus/modbus/functions/mbfuncinput.c
SOURCES += sources/libraries/freemodbus/modbus/functions/mbfuncother.c
SOURCES += sources/libraries/freemodbus/modbus/functions/mbfuncdisc.c
SOURCES += sources/libraries/freemodbus/modbus/functions/mbutils.c
