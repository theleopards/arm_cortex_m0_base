#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

extern void uart1_init(uint32_t baud);
extern void uart1_putc(uint8_t c);

#ifdef __cplusplus
}
#endif

#endif // __IO_CFG_H__
