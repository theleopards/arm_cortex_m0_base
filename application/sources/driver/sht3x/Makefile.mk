CFLAGS		+= -I./sources/driver/sht3x

VPATH += sources/driver/sht3x

SOURCES += sources/driver/sht3x/i2c_hal.c
SOURCES += sources/driver/sht3x/sht3x.c
SOURCES += sources/driver/sht3x/sht3x_system.c
